import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// layouts
import Header from "./components/layouts/Header";
import Footer from "./components/layouts/Footer";
// pages
import Main from "./components/pages/Main";
import Home from "./components/pages/Home";
import Video from "./components/pages/Video";
import Account from "./components/pages/Account";
import Auth from "./components/pages/auth/Auth";
import PostItem from "./components/pages/PostItem";
import NotFound from "./components/pages/NotFound";
import Welcome from "./components/pages/auth/Welcome";
import ProtectedRoute from "./components/pages/auth/ProtectedRoute";

const posts = [
  {
    id: 1,
    title: "Kakashi Hiden: Lightning in the Icy Sky",
    description:
      "Nearly a year after the end of the Fourth Shinobi World War, Naruto Uzumaki and Sai are on a mission in the Land of Waves to capture Garyō, leader of the Ryūha Armament Alliance. The Ryūha Armament Alliance has been following the example set by Madara Uchiha, and in doing so has cost the lives of several of the Land of Waves' citizens. When they locate the group's hideout, Naruto attacks with his Multiple Shadow Clone Technique. He easily deals with most of the enemy forces, but has difficulty with a masked bodyguard that uses Ice Release. Naruto distracts the bodyguard with a shadow clone for long enough to apprehend Garyō.",
    img: "https://media.giphy.com/media/uA9MR3iP3590Q/giphy.gif",
    updatedAt: "3 mins ago"
  },
  {
    id: 2,
    title: "Shikamaru Hiden: A Cloud Drifting in Silent Darkness",
    description:
      "Two years after the end of the Fourth Shinobi World War, Shikamaru Nara lies on the roof of the Hokage Residence in Konohagakure, wondering how his life went so awry. Once, he wanted to spend his days watching the clouds, having an unremarkable life that would end with an unremarkable death. Starting with his promotion to chūnin, however, he was increasingly heaped with responsibilities, culminating with his most recent job of advising the five Kage and coordinating missions for the Shinobi Union, the successor to the Allied Shinobi Forces. Shikamaru decides that he won't get up until somebody else forces him to.",
    img: "https://media.giphy.com/media/Mj0gk1wnekXC0/giphy.gif",
    updatedAt: "3 mins ago"
  },
  {
    id: 3,
    title: "Sakura Hiden: Thoughts of Love, Riding Upon a Spring Breeze",
    description:
      "Sakura Haruno reflects on some moments she's had with Sasuke Uchiha: when she unintentionally insulted him shortly after the formation of Team 7; when she tried to stop him from defecting to Orochimaru; when he left Konoha to wander the world, but promised to see her again. Sakura wonders where he is now and what he is doing but has hope he will return.",
    img: "https://media.giphy.com/media/EETZoyWldXgJ2/giphy.gif",
    updatedAt: "3 mins ago"
  },
  {
    id: 4,
    title: "Konoha Hiden: The Perfect Day for a Wedding",
    description:
      "Kakashi Hatake, the Hokage, receives an invitation to Naruto Uzumaki and Hinata Hyūga's wedding. Kakashi has every intention to attend and he wants everyone else who was invited to be able to go as well. However, because Naruto and Hinata's friends have all risen to positions of importance within Konoha, all have vital and overlapping missions that they must be available for. Although Kakashi can in theory rearrange mission schedules in such a way as to make everyone available on the wedding day, he can't account for unforeseen circumstances that may prevent individuals from returning in time. He contemplates making the wedding itself a mission so that everyone can be \"assigned\" to it without problems, but worries that would be an abuse of his authority.",
    img: "https://media.giphy.com/media/sublGtus7Eali/giphy.gif",
    updatedAt: "3 mins ago"
  },
  {
    id: 5,
    title: "Gaara Hiden: A Sandstorm Mirage",
    description:
      "Gaara, the Fifth Kazekage, is still having to deal with a lack of funding for Sunagakure. Although the Land of Wind was happy to pay for all expenses that led to victory during the Fourth Shinobi World War several years earlier, the Wind daimyō has been reluctant to fund issues that arose as a result of that victory: compensation to families of shinobi who died during the War or rehabilitation costs for those who were injured. It's currently Gaara's job to try and get the money for those issues from the daimyō. While he contemplates these issues, Gaara is visited by his older sister, Temari. Temari is to be married to Shikamaru of Konohagakure's Nara clan; their relationship surprised Gaara when he first learned of it and, despite his research, he still doesn't really understand it. Because Temari is a sibling of the Kazekage and Shikamaru is an influential figure in Konoha, their marriage is a delicate political matter that will solidify peace between their two villages. In the midst of careful consideration, Temari and Shikamaru are trying to find a date for their wedding that will appease both sides.",
    img: "https://media.giphy.com/media/62LFIeOGuBqM/giphy.gif",
    updatedAt: "3 mins ago"
  },
  {
    id: 6,
    title: "Akatsuki Hiden: Evil Flowers in Full Bloom",
    description:
      "Several years after the end of the Fourth Shinobi World War, Sasuke Uchiha travels through a forest. Despite having once been an international criminal, he was granted freedom and a new outlook because of the friendship of Naruto Uzumaki, the love of Sakura Haruno, and the trust of Kakashi Hatake. He now seeks to redeem his past actions by, among other things, looking at the world with new eyes. As Sasuke emerges from the forest, he sees a patch of white flowers growing at the base of a nearby tree. Because he likely would never have even noticed the flowers only a few years ago, he stops for a moment to appreciate them.",
    img: "/img/img-2.jpg",
    updatedAt: "3 mins ago"
  }
];

const App = () => {
  return (
    <div className="container-fluid px-0">
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Main} />
          <Route
            exact
            path="/home"
            render={props => <Home {...props} posts={posts} />}
          />
          <Route
            path="/posts/:postId"
            render={props => <PostItem {...props} posts={posts} />}
          />
          <Route exact path="/video" component={Video} />
          <Route exact path="/account" component={Account} />
          <Route exact path="/auth" component={Auth} />
          <ProtectedRoute path="/welcome" component={Welcome} />
          <Route path="*" component={NotFound} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
};

export default App;
