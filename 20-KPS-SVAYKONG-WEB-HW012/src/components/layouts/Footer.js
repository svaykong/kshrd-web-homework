import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faTelegram,
  faInstagram,
  faLinkedin
} from "@fortawesome/free-brands-svg-icons";

const Footer = () => {
  return (
    <div className="bg-light text-center pt-3 fixed-bottom">
      <h3>Copyright, &copy; Svay Kong, 2020</h3>
      <h5>Contact Us:</h5>
      <ul className="list-inline">
        <li className="list-inline-item">
          <a href="#facebook" className="btn disabled text-dark">
            <FontAwesomeIcon icon={faFacebook} size="5x" />
          </a>
        </li>
        <li className="list-inline-item">
          <a href="#telegram" className="btn disabled text-dark">
            <FontAwesomeIcon icon={faTelegram} size="5x" />
          </a>
        </li>
        <li className="list-inline-item">
          <a href="#instagram" className="btn disabled text-dark">
            <FontAwesomeIcon icon={faInstagram} size="5x" />
          </a>
        </li>
        <li className="list-inline-item">
          <a href="#linkedIn" className="btn disabled text-dark">
            <FontAwesomeIcon icon={faLinkedin} size="5x" />
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Footer;
