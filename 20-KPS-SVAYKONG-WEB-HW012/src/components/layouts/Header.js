import React from "react";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../../styles.css";

const Header = () => {
  return (
    <>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/" href="#main">
          React Router
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link
              as={Link}
              activeClassName="active"
              to="/home"
              href="#home"
            >
              Home
            </Nav.Link>
            <Nav.Link
              as={Link}
              activeClassName="active"
              to="/video"
              href="#video"
            >
              Video
            </Nav.Link>
            <Nav.Link
              as={Link}
              activeClassName="active"
              to="/account"
              href="#account"
            >
              Account
            </Nav.Link>
            <Nav.Link
              as={Link}
              activeClassName="active"
              to="/auth"
              href="#auth"
            >
              Auth
            </Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default Header;
