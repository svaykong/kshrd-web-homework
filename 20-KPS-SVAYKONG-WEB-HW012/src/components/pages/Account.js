import React from "react";
import { Switch, Link, Route } from "react-router-dom";
import queryString from "query-string";

const accountTypes = [
  { id: "netflix", name: "Netflix" },
  { id: "zillowgroup", name: "Zillow Group" },
  { id: "yahoo", name: "Yahoo" },
  { id: "moduscreate", name: "Modus Create" }
];

const AccountItem = props => {
  const query = queryString.parse(props.location.search);
  return query.name === undefined ? (
    <h3>There is no name in the query string</h3>
  ) : (
    <h3>
      The <span className="text-secondary">name</span> in query string is: "
      {query.name}"
    </h3>
  );
};

const Account = () => {
  return (
    <div className="container mt-3">
      <h2>Accounts</h2>
      <ul>
        {accountTypes.map(type => (
          <li key={type.id}>
            <Link
              className="text-decoration-none"
              to={`/account?name=${type.id}`}
            >
              {type.name}
            </Link>
          </li>
        ))}
      </ul>
      <Switch>
        <Route path="/account" render={props => <AccountItem {...props} />} />
      </Switch>
    </div>
  );
};

export default Account;
