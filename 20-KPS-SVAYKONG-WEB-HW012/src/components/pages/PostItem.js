import React from "react";
import { Row, Col, Card } from "react-bootstrap";
import NotFound from "./NotFound";

const PostItem = ({ match, posts }) => {
  const postId = Number(match.params.postId);
  const post = posts.find(post => post.id === postId);
  if (!post) {
    return <NotFound />;
  }
  return (
    <div className="container" style={{ marginBottom: "220px" }}>
      <p className="my-3">This is a content from post: {postId}</p>
      <Row>
        <Col lg="5" md="5" sm="12" className="m-auto">
          <Card>
            <Card.Img
              variant="top"
              src={post.img}
              style={{ width: "100%", height: "40vh" }}
            />
            <Card.Body>
              <Card.Title>{post.title}</Card.Title>
              <Card.Text>{post.description}</Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">
                Last updated {post.updatedAt}
              </small>
            </Card.Footer>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default PostItem;
