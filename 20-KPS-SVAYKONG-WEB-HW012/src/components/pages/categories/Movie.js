import React from "react";
import { Switch, Route, Link } from "react-router-dom";

const Movie = () => {
  return (
    <div>
      <h2>Movie Category</h2>
      <ul>
        <li>
          <Link className="text-decoration-none" to={`/video/movie/adventure`}>
            Adventure
          </Link>
        </li>
        <li>
          <Link className="text-decoration-none" to={`/video/movie/comedy`}>
            Comedy
          </Link>
        </li>
        <li>
          <Link className="text-decoration-none" to={`/video/movie/crime`}>
            Crime
          </Link>
        </li>
        <li>
          <Link
            className="text-decoration-none"
            to={`/video/movie/documentary`}
          >
            Documentary
          </Link>
        </li>
      </ul>
      <Switch>
        <Route
          exact
          path="/video/movie"
          render={() => <h3>Please select a topic.</h3>}
        />
        <Route path="/video/movie/:movieId" component={MovieItem} />
      </Switch>
    </div>
  );
};

const MovieItem = ({ match }) => {
  const { movieId } = match.params;
  return (
    <div>
      <h3>{movieId}</h3>
    </div>
  );
};

export default Movie;
