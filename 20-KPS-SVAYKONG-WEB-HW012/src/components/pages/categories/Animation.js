import React from "react";
import { Switch, Route, Link } from "react-router-dom";

const Animation = () => {
  return (
    <div>
      <h2>Animation Category</h2>
      <ul>
        <li>
          <Link className="text-decoration-none" to="/video/animation/action">
            Action
          </Link>
        </li>
        <li>
          <Link className="text-decoration-none" to="/video/animation/romance">
            Romance
          </Link>
        </li>
        <li>
          <Link className="text-decoration-none" to="/video/animation/comedy">
            Comedy
          </Link>
        </li>
      </ul>

      <Switch>
        <Route
          exact
          path="/video/animation"
          render={() => <h3>Please select a topic.</h3>}
        />
        <Route path="/video/animation/:animationId" component={AnimationItem} />
      </Switch>
    </div>
  );
};

const AnimationItem = ({ match }) => {
  const { animationId } = match.params;
  return (
    <div>
      <h3>{animationId}</h3>
    </div>
  );
};

export default Animation;
