import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSmileWink } from "@fortawesome/free-solid-svg-icons";

const Main = () => {
  return (
    <div className="container text-center mt-3">
      <h1>This is Main Page for you.</h1>
      <FontAwesomeIcon icon={faSmileWink} size="10x" color="#263238" />
    </div>
  );
};

export default Main;
