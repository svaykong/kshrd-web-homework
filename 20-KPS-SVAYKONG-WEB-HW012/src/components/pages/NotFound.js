import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFrownOpen } from "@fortawesome/free-solid-svg-icons";

const NotFound = () => {
  return (
    <div className="container text-center mt-5">
      <h1>404 Page Not Found</h1>
      <FontAwesomeIcon icon={faFrownOpen} size="10x" color="#263238" />
    </div>
  );
};

export default NotFound;
