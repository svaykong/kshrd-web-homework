import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReact } from "@fortawesome/free-brands-svg-icons";
import authController from "./AuthController";

const Welcome = props => {
  return (
    <div className="container text-center mt-5">
      <h1>Welcome {props.location.state.user.name}!</h1>
      <FontAwesomeIcon icon={faReact} size="10x" color="#263238" />
      <div className="mt-5">
        <button
          className="btn btn-dark"
          onClick={() => {
            authController.logout(() => {
              props.history.push("/home");
            });
          }}
        >
          Logout
        </button>
      </div>
    </div>
  );
};

export default Welcome;
