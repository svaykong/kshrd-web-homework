import React, { Component } from "react";
import authController from "./AuthController";

class Auth extends Component {
  state = {
    name: "",
    password: "",
    nameError: "",
    passwordError: ""
  };

  // handle change
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // validate form fields
  validate = () => {
    const { name, password } = this.state;
    let nameError = "";
    let passwordError = "";
    const regexName = /^[a-z][a-z\s]*$/i;
    if (name.length === 0) {
      nameError = "Name can't be empty!";
    } else if (!regexName.test(name)) {
      nameError = "Invalid name!";
    }
    if (password.length === 0) {
      passwordError = "Password can't be empty!";
    } else if (password.length < 6) {
      passwordError = "Password can't be less than 6 characters";
    }

    if (nameError || passwordError) {
      this.setState({ nameError, passwordError });
      return false;
    }

    return true;
  };

  // handle submit
  handleSubmit = event => {
    event.preventDefault();
    const isValid = this.validate();
    if (isValid) {
      const { name, password } = this.state;
      const user = {};
      user.name = name;
      user.password = password;
      this.setState({
        name,
        password,
        nameError: "",
        passwordError: ""
      });
      authController.login(() => {
        this.props.history.push({
          pathname: "/welcome",
          state: {
            user: user
          }
        });
      });
    }
  };

  render() {
    const { name, password, nameError, passwordError } = this.state;
    return (
      <div className="container text-center">
        <form onSubmit={this.handleSubmit}>
          <h3 className="my-3">Please Log In</h3>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="name"
              value={name}
              onChange={this.handleChange}
              placeholder="Username"
            />
            <h5 className="text-danger">{nameError}</h5>
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="password"
              name="password"
              value={password}
              onChange={this.handleChange}
              placeholder="Password"
            />
            <h5 className="text-danger">{passwordError}</h5>
          </div>
          <button
            className="btn btn-dark"
            type="submit"
            onClick={this.handleSubmit}
          >
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default Auth;
