import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Animation from "./categories/Animation";
import Movie from "./categories/Movie";

const Video = () => {
  return (
    <div className="container mt-3">
      <ul>
        <li>
          <Link className="text-decoration-none" to="/video/animation">
            Animation
          </Link>
        </li>
        <li>
          <Link className="text-decoration-none" to="/video/movie">
            Movie
          </Link>
        </li>
      </ul>
      <Switch>
        <Route
          exact
          path="/video"
          render={() => <h2>Please Select A Topic</h2>}
        />
        <Route path="/video/animation" component={Animation} />
        <Route path="/video/movie" component={Movie} />
      </Switch>
    </div>
  );
};

export default Video;
