import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

const overFlowText = {
  overflow: "hidden",
  textOverflow: "ellipsis",
  display: "-webkit-box",
  WebkitBoxOrient: "vertical",
  WebkitLineClamp: 4
};

const Home = ({ posts }) => {
  return (
    <div className="container-fluid px-5" style={{ marginBottom: "240px" }}>
      <Row>
        {posts.map(post => (
          <Col
            lg="4"
            md="4"
            sm="12"
            key={post.id}
            style={{ marginTop: "35px" }}
          >
            <Card>
              <Card.Img
                variant="top"
                src={post.img}
                style={{ width: "100%", height: "40vh" }}
              />
              <Card.Body>
                <Card.Title>{post.title}</Card.Title>
                <Card.Text style={overFlowText}>{post.description}</Card.Text>
                <Link to={`/posts/${post.id}`} className="text-decoration-none">
                  See More
                </Link>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  Last updated {post.updatedAt}
                </small>
              </Card.Footer>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default Home;
