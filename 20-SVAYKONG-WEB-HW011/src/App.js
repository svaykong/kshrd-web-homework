import React from "react";
import { Card, Form, Button } from "react-bootstrap";
import CardItem from "./components/CardItem";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formControls: {
        number1: "",
        number2: "",
        calculator: "plus"
      },
      data: [
        {
          result: ""
        }
      ]
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(event) {
    const value = event.target.value;
    const name = event.target.name;

    this.setState({
      formControls: {
        ...this.state.formControls,
        [name]: {
          ...this.state.formControls[name],
          value
        }
      }
    });
  }

  onSubmit(event) {
    let number1 = this.state.formControls.number1.value;
    let number2 = this.state.formControls.number2.value;
    let result = "";

    if(number1 === "" || number2 === "") {
      alert("Cannot calculate number");
    }

    else if (isNaN(number1) || isNaN(number2)) {
      alert("Cannot calculate number");
    }

    else {
      number1 = Number(number1);
      number2 = Number(number2);
      if (
        this.state.formControls.calculator === "plus" ||
        this.state.formControls.calculator.value === "plus"
      ) {
        result = number1 + number2;
      }

      if (this.state.formControls.calculator.value === "minus") {
        result = number1 - number2;
      }

      if (this.state.formControls.calculator.value === "multiple") {
        result = number1 * number2;
      }

      if (this.state.formControls.calculator.value === "divide") {
        result = number1 / number2;
      }

      if (this.state.formControls.calculator.value === "modulus") {
        result = number1 % number2;
      }

      this.setState({
        data: [...this.state.data, { result }]
      });
    }

    event.preventDefault();
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-5 col-sm-7">
            <Card className="mt-3">
              <Card.Img variant="top" src="../../img/calculator-img-2.png" />
              <Card.Body>
                <Form noValidate onSubmit={this.onSubmit}>
                  <Form.Group>
                    <Form.Control
                      name="number1"
                      type="number"
                      value={this.state.formControls.number1.value}
                      onChange={this.onChange}
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Control
                      name="number2"
                      type="number"
                      onChange={this.onChange}
                      value={this.state.formControls.number2.value}
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Control
                      as="select"
                      name="calculator"
                      className="text-primary"
                      value={this.state.formControls.calculator.value}
                      onChange={this.onChange}
                    >
                      <option value="plus">+ Plus</option>
                      <option value="minus">- Minus</option>
                      <option value="multiple">* Multiple</option>
                      <option value="divide">/ Divide</option>
                      <option value="modulus">% Modulus</option>
                    </Form.Control>
                  </Form.Group>

                  <Button variant="primary" type="submit">
                    Calculate
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </div>
          <div className="col-lg-4 col-md-5 col-sm-7">
            <h5 className="mt-3">Result History</h5>
            {this.state.data.map((value, index) => {
              if (value.result.length === 0) {
                return "";
              } else {
                return <CardItem key={index} result={value.result} />;
              }
            })}
          </div>
        </div>
      </div>
    );
  }
}
