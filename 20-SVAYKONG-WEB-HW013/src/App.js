import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// layouts
import Header from "./components/layouts/Header";
// pages
import HomePage from "./components/pages/HomePage";
import PageNotFound from "./components/pages/PageNotFound";
// articles
import Articles from "./components/articles/Articles";
import ViewArticle from "./components/articles/ViewArticle";
import AddArticle from "./components/articles/AddArticle";
import EditArticle from "./components/articles/EditArticle";

export const App = () => {
  return (
    <div className="container-fluid px-0">
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Articles} />
          <Route exact path="/home" component={HomePage} />
          <Route exact path="/articles" component={Articles} />
          <Route exact path="/articles/view/:id" component={ViewArticle} />
          <Route exact path="/articles/add" component={AddArticle} />
          <Route exact path="/articles/edit/:id" component={EditArticle} />
          <Route exact path="*" component={PageNotFound} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;