import React, { Component } from "react";
import Axios from "axios";
import moment from "moment";
import { Table, Image, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../../styles.css";

export default class Articles extends Component {
    state = {
        isLoading: false,
        articles: [
            { 
                ID: 123456, 
                TITLE: "Nancy", 
                DESCRIPTION: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", 
                CREATED_DATE: "2020-06-11T03:56:13.324Z", 
                IMAGE: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSKhYdsTSKPcmiaJp1HlqcHkJhf88GWG1ziIMcVS_oOdZqtpcEA&usqp=CAU",
            },
            { 
                ID: 123457, 
                TITLE: "Rose", 
                DESCRIPTION: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", 
                CREATED_DATE: "2020-06-11T03:56:13.324Z", 
                IMAGE: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSEhIVFRUXFxcYFRgVFRUVFRgYFhYXFxUVFxcYHSggGBolGxYWITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0lHyUtLS0tLS0tLS0tLS0vLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOAA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAAIDBAYHAQj/xABAEAABAwEEBwYEBAUEAgMBAAABAAIRAwQFITESQVFhcYGhBiKRscHwBxPR4SMyUmJCcoKS8RQVM6KywjRT4hb/xAAZAQADAQEBAAAAAAAAAAAAAAABAgMEAAX/xAAmEQACAgICAQQCAwEAAAAAAAAAAQIRAyExQRIiMlFxBEITFGGB/9oADAMBAAIRAxEAPwDuKSSS44SSSS44SRSVO9rV8umTrOA4nJBulYUrdGdv626VTPutmOWfvgslflu0KTnHMz/hELTVkk+HBpjqZWJ7ZW0kNpzOkZ5bOizL1SN3tiCLvYa1STjC6Xcl3BrQsz2PuzAEhdEstGAmlthjpHlOkpNFSwmwlDY2F4QnrwhcEjIUdRshTkJhCAQJedjDmkQsZeVgOMDEZ7/YXR61NAL2ssd6J2oXQas5nXo4xt15cj9VEzB0e8c+S0t6XYCTGsS1Z+tS1HMeWr6K8ZWZpwotU6gcId7H1HolZbQWPaScjB3g6/exVGOPMf5CsVWSNLd01+H0TCBGm7QrbneupWWHNp5fXyQmrWlrHawYPvlPNEnvxB2z78kA2aDsrePy6wk4H1z9V12y1NJoK4NZHkPG0Ejzhdh7K275lMbYB6f58EOJAmrj9B5JJJVM54kkkuCepJJLgCSSSXHCWS7ZXhALRmMB/M7Bam0VQ1pcdQXLrwtfzbQ7HCnBO9xny9FHK+jRgjuxlsqw06tU7gMT5+Cwgm0WkRkIA3DUtD2mtejQJ/UNEc8+gPis/wBla5Y4vAk/RLBabLS20jqdy2AMYAjLVi7N2iq4dxE7Ff4cYIgoFPFs0JKao6NXSEqWEAcHgSXqjqVQMygFDl4QqpvCn+sJNt9P9Y8UdHOydzVRtlGQcFebVaciCm1WINBTMVbqGiY2GRwPuUDve78Zjb1WwvqzYT7hDH09OmHcncdaVOhmrRgSwjDWOvuVLQqQdHVq4K7e9lgnn78EMa6YOv6LSnaMclTH1O7LfDl9kWpv/Da7+X6+nVA7XUyOzA+/eaL2d34DUzAi2xsPO4tPoV0DsTboc1pOekP7cfUrBAS4H9o8wCjtyWrQew6hUaeTgpsdfB2NJRWZ8tadylVTIzxJJJEJ6kkkuAJJJeErjjPdtLyFKjGs5ei5zZTFNx1v8SXZe96Jduby+bUP6QYA2gZxxxHNDGyPltOcgn19Fkk72b4R8VRnu29oxZTB/KJ5uhHuyV0sDASMVjO0Fo07SdgPQHBa2579p06YkgRtcAndqKDCnJmsdYWbFH/trSZGaDN7YWYmDUaN+kIRqw25rwC1wIORBkKdvstS6YWu6mQIKuuKr2V8qzVTEnyRPcgVvY5844I1VVOo4BK2PFGffYCcpXlK5nZz44nqjXzQniqu8hvFgkWSozIn3wVqhebm4PGCJMqhe1LM14xAXAf+lesW1G4YoHYmwX0zvjliPEEeCLm7S3FhQq1tc14dEGcfT1HNAIAv6hAn3I+xWQqiCY2+/e5dIviz6TTv9QueXjS0XbsjwVsT6M2ZdlK01ZRmxumzkbP/ANLO1neqO3OZpEe8SrS4IRew1YHaTaZ3EeA+ynZUhhI1Fn/Vx9FQuB0tH7XeYIPkpqxhlQe8FPsp0dv7OWj5lnpu2jrrRNZH4bWvTs2ic2nocVrk8eCGT3M8SSSTCnqSSS4AkK7SW75VBx1nAYxmiq5r8S7277aQdlmBswn6JMj1RXDG5fRkjU+ZWJOTSOGBwPQeKndU78jU1x8MAqV2GKYJH5jPjgOgHipS/wDDqu/bHqsz5NqMba2kkujEknl7lE+ylkY6pNYAjVOSnuy7zUWnuy4g3VIV3Oiccd7M72yu7RqkhoDSGlmAAIAALRznDeFV7L1KtKsNDBhMvpgktEkAROTp6LfvuRrhBaSNhJjwVuw3AxhBDGiMoA8ZS+Vqhljpp2FbCTARKoq1mo4hWqxSUFvZTqlAL7topNLjl7wR54QW+rtFVoBnAzgY1EeqSisXRlrbeNpNF1dtOKbRMl0TjGQxOe5Z4dp6zdIu0Bo5jEOPDFbynRc2k6iQHscCCHYGDvH0WTqdlnh0iDqBIkwNoGB4qq8F0TksremXrn7XzAfI/my3w76rZWG82vEgrIOuPSptpFpDWyRhiSczO1XLruF9P8j3t3HEeCRpdFF5fsbim+QqF60JHvapbAxwADuis2lkhADVMzGbSw6vL7ZeKwV90+84Lf3kz5Znly9+qwnaH/kO9Nj9xPL7TMWgYoxcru4eB6BC7S3FErkOHvWFqlwYo+4L3Ng+o3+ocjPlKnvB3/JvHUGPfFVLufFZh1EaJ6t+iIXvTieHmRJ8VL9i36m0+E1rzb+pvVv2JXSlxj4cW3Qqt2TB559F2cJ1y0RyLhniSSSYQ9SSSXAKt5WsUqTqh/hErgd62x1euXOOL3Hk0A++a6V8T73DaYotd3nHIeq5TYnRp1cwAQ3fGzicOZ2qMnbs1Y41H7CdWtBw/hHv3uXtYRQfznkHH1Coh5gAnHCd5iT1lXa5/Bfw8mx6KTRdOwp2as3dC2Vms+CznZRssadwWzosR7GekMp0FMGJ69ZiiI2e02QJUVYqw8qpVKLOihgavDTSY5ShKMUqtjGxRiwhFNFeCmuO8iiyxhTts4VkMXoC47yZG2kvXswT01xXAM/ftn0mHaPYK5bfJM45jArr15ZHguVdoWRUIRx+47L7TOWhELm1cfoqNcYwr91CBz+i0vgxpbLNF+W50+I/x4o/efeZpbj0gjos5QP5vez6LQ2d2nRI2CeQEHopy00ysdpo87KP0an9QK7jdtbTptO6Dy99VwW5zGOzA8vfRdl7J2nSZG4EeR/9ULqYJxuH0HkkklUznqZVeACSYAEkp6yvxAvQ0qBY0w6phOwa+aWTpBhHydHMe1lsNptD3A4ZT+luwbzKEVagwY3ANOHAKa21A0mBwGcSNe+Jw1Y7UOqHRw16/DL3tUkjY2WdLBx3n1RSv/xOG2es/ZBRU/Ny+qLOdNM8GnxKWSDFmp7C96m3gPJbZoWI+Hzu5o7CR4FbkrhpENRylsze7OsiVBWVG869cMY6ji5hBLdTwM28xr1IXQVG9F5z0wuTH1tIB0ESMjmNxWQvu9rax5dZmNLWn8jhJeBnjMidUdUL3Q6jqzWPbGIU1F8qnZLZ8yk15aWFzQS135mkjFp3hWLJkFyOa0XGp4Ca1SBMSYoTSE5NK44aQoqhUpKq2hyVjIGXrVhpXNr3p6TydS3N+1u6QsZeh0WE7P8AARgdPgy7mYndnyVmyOw8YUDnHEbfopqOAWgyklF35kbuOsBAOIyPBZ9jo6Ty9lELuq6J4EHwj7oTWgwewnQp6Lnt2OXQexNqhzQTrLeRy6wsRWp/iB2pzR4jA+iO9na8HDPMcWkYdFFvsslpo6wkhn+7s/UElfyRk/jl8BQrkfxBvEVK0AnSaS0DUANcbfqV0ntFbvk0HvmDEDiVxyrTNWo0Ti6oADO0wTOoS6Us3bophVJyBtuaGU2E5mXHPMwAOng1BnPwnb9z5SUb7XVmmuWMPcZDW8G4at0eJWZtVbEAILY8nRfpOz35IvZHzSdGYHrggdmfgil3PGI1OIb4iPPzSzQ0Waf4dV++9u8EcDj9V0ghcj+H1eLZoHW0/wDU5dV16MErQ1lYtTS1SuQm23gZhvigVx45ZHSH2p0mEynRCF2m8XNgkTOtKzXwJ72CVmr+tkS0GG0QrVMKtZ64cJBVphRRlla0ywxPlRNKdKIjHFy8JXi8JXAGvKpWlytVChtufAKRlIozd+VpMILbqcNx1+Zw9Vbfa2PtGgXZCYVTtFWDSDMASTyH1ITR5o58NmPtR0nuOqfJSvMABV6elpFpkYyRvMEdFKTJPvIH6LSY77EdW8+qt2Z2J4KmXY+H0+qtWbWiwLk1FjqaVNp2YHw+48EQu+poljt4M7sfuhFyOmmW8Oul9kTszpaIykjlmFmkaYmulu1JCfmH9XRJJQ9oM/Em3kUxTaTgRpRMy78ow3A+IWX7O2HSNZwOFFjWtA/L8x8NkasIcduAKtX/AFxVt1QTosYH1HO2Opt0WnaQDBhFfl/Iu4OA7zhpzrlwhsnX3R1WnpsyrVROSXtVGm4jWek/dAnPlwPvFEb2f3oGQHohBfjzCaK0Cb2ErLVyHvYtj2As4q2iSJFNhcQdo7rerp/pUXw2umnWbaH1GNfohrWhwBgnScSJ14Bby6bop2WmWsbDnHSedc6m8BlClOSTotjxtqzn9yu+TetNpy+aW/3BzY8YXaaq4L2grGneDXDP51N3g5pHvcu6UrQHtwz1jWEHwjmmpMz3aO+DREgEjIwJ5rCW7te35gZ3hImdEnpmumWy7RUkFZi39m6YJJbik+z1PxZJrxi0n9WUza6ZphxtVMgCcS2ROMETIKrWxwa3SFamWgT+YTyiZXr7nGonzUD7jG7DcENfBtWPMv3T/wCE93X9oEQ4HmD5LY3TebKolpxGY2LCu7NtfnJ4CPJaTspcIswMTLtpJgDIYrtGP8ta9VX/AIathTwVFTT5Rs88cSmkrwlROeg2FI8qvWfv22BjCScgilqrwFhO0ls03aAOGbvQJVtjPSMZe9R2l8wEh0zIMEJrrQ+sJLnn8KXQCQS1wku3DbvC9vZ40gCJEiRt3LyjWmmdFrWkaQJEkvBcBAE4AAjwWuPFmGTd0OpPJBe4ySMfovbMNvHzTa+DQ337+ikojPl9/NEBGM/CVboZDj6fdU6OLuZ9+CtN8h6rmdEP9nXZjfHhj6ItYiNF7dhnyPogvZ90H+o+RRewQRU4HylQnyaIcF7/AFTv09CvUM+adpSS0Uskuyh8+s/TxL6gYQMyHOgjYQGtcZW77Z//ABgBAwJA/aGw0dR4IB2Es4qWqoaYLadM5OgkOcNETyDzG9F/iHWIpVC3JjNH+6CekeKu9RMq3M4ReLu8UNqIleYhx4n7eaGvKdcCT5OxfCS7iLK6oThVqaQH7afd6ulai9DCrfD2h8u7bPObqc8nEuHmrlSj81xBwaM/osubbNv47pX0Yu4+zwtFtfbKzJp0iG0mnJ9UY6R2tbI4ngtebXo12mfz908gSD06qau5rG6LRotGQCyFa9GutLWh2LDJHHDDbHqllLhLovGCpyfZ0EOVa2UQ5NsdaQFacxPdozpuEtAJ91jUV4LrjWizmJmglNP9nJXJUo2VrVapsXuintQIyk29jmr0uTCVG965s5Ie56rV60JlWvCD2+3ahiUljpUVr7vKBDcScllazYBJ570Xq0sycSUKvJ0BNESRlbc8fMBIkAyRlO5S2aoThgQXE5NDtUaRAk8OKjov77nnKHYxIbgQDHHzTbL3WE8t61rgxPk9e+XE+CtNMAnZn5keapWcSfexWrQ7Rp7z/hEB5Z2QI4j31VgOz8PDBNotjlj4An1CVIeceEA9UGFBq4xiP5kWuo/mG49Ahd2CG6WvPxMDrCv3Y+PmO2B//iQoy2WiO+aN6SraBSSjHR/hzYwygHNJJqEvJIg45TyAVD4m1DTslT97mjfi6T/4rZ3LZBTosbH8I36lzL4q2zSbTpgmHVC6AdkNGHFx8CtMjLDbs5fb2yBwVS7LvfaK9OhT/NUeGjd+px3BoJ5IhesBxjIEgbY2K98OrQGXlZyf4nOYf62Oa0jfJHVdF6OmtncRQFOnToUh3Wtaxg3NECeQXtWmGNDRzO06yrAbHeOcQEPttoiVll8s2wXCXBku3V+GhTAb+Z50Qf07XcvMhZTt5ZnWP/RFjgHilpkYzLzpODtoxTLXVN4XrTojGm1+jhiCGd6oeZEcltu2HZkW63NdUltnosa10YF7s/lt2CIk74GJkVhBRjb7JZZuc/GPR52O7RMtFIOaYIwe3W07PutlSqghcb7bWZl3WmlXscU/maQfSklhDdHVqGOWrCNa13ZPtay0MDoLDkQ7br0T/EFNqtrgqtvxlybVwTCoKdsBTnVgh5BUWOcmlyhfXCqV7cBrSNjqJbqVlSr2uNapVbSTkq5YTmlsaqPbRaS7JVhTVptNNeFwCjXCyPaGtqnPoFrbc+GlYK+KsuJOfs/RUxLZLK6RRYJZUJB0Ro5ZTpYNPET4J9q7rANcSeJz9VBYa0v0Dix35hGB0QYO6JKmtZJdjt9JWrsx9EtlZ798FLbG4tG8eAgnqUrCMfD1leWt0v4NnmSQPMLuw9E1H8rnbp8ST6dUqDcBwHicfTqvKohrWD+I9MvUKzZWaT4HvV6JWwpBdh0WAbp8Bh1U1gaS3Qbm9wb4u/z4qjazMgHdykDqdIotcjwxr67sqYkcY7sdPFTfBVcms/2ensXqzXz6v60kvgN5s7FbamjTcdgw8lxHtfaxWtgDcqbIPFjSXH+49F13tXahTs73ExgcturquDOqy6q87I/uMlWm9mfEtWA7yOfHD1RD4fUG1LxswcSIeXYT+amxz2iR+5o8taD2+pjzU/ZiuadtsrwY/HpeDqjWuHgSnitCyez6HrVcFnL3tBDSNuC0lqpxKBWyx/NimMC457BmT4LFJM9HG1RmPhv2bdStFW1OADBLaJOZLiC4gbBlK3FstOiCSdpk9TKmNFrGtYwQ1oAA4eq5r8Wb7exjbMzD5gJe79ow0BxJx3Deq25NImlGCcqMN20vr/VWlz2mWN7tPYQM3DiZ5QtjcNmDaNNsfwieMSVzIrq91Dut4BHP6YpIH4b8skpMturPa3uucOaiffFcR38ODfordRuCDUnBzTGokeBhQiac2mgtQtj3fmcTzV6iEDsbkbsxQYE9FprE7RSYnErgWMIUNVTOKqV3rgMEXtVwKwd7VZc4jgOXsrXdobRDd/qsReOAC0YkZszGXWMXO2CBz+0qes8l07j6Aeqju8QwnafLL1Tm5n+kdSSr9mZcF+wnPgOuHqmOxqneQOn1AUljGB4tHqo7OZeTxPMmP/VKP8FhxmpOprR76hW7E7RaXDMwG7pgTyz5Ie45/ucRymPIK5ZzLmt2QOmPmlYUEaDJeZwAHgAIHPMrR2673f6X5QHeOi4jgcG+CDXHQdUrmBIG3LMAdAf7l0SxWFzmw+JJgb5ClK7Kxox0bj4FJbH/APmj+joEl1sPp+Qb8T73JaKYMCcRt1rmNZ+jSO1x8kc7Y3h82udfvFZm315IGoT4qi3sm/ToEWkyVDTqlrg9v5muDm8WmR1Cc85n3inWFxFRrhgQZB2EZFWRnez6YNQVGNqDJ7Q4cHCfVULA38V25p6kKLsbeTrTYmVHCDL24bGuIYf7NFX7EyC88B5rNJeo245ehkVUrlPxIu81ya7CSaYIjUWgkuI3+cLrFoGBWMtNLvuGokqfk4u0WUVOLTOJTrXV7j79NjpiQD4hYLtJcL7O4uiaTidE7Mfylbzsr/xMGxo8gq52pRTRH8VOE2mGHWbDErH9m7RL61M5tqv6uK3jxgsDUomlbamxxDvH7yo4+0Xz3ph7Qh0opZXKmwaQBU1nEIMEWFWlIuUTUpQCOcqVqerLnIXeVSB7zJw+vJEDM3fVXSdHvD30WYvc4xwWjvFw0p2YDl7KzF5YvjWf8BacZlyk1HBrRuJPPJOYMOLvLBI5ujIQByXrTgPeZlUJFxj4YTsM/wDX7JlExpnWPQT5rwH8ODrz8vVOaIEbST4mfqlCeTB/lHnh74q5drofP7j90OqnCdpnwyV27j5DkZlczlybHsy7R1d7SbJOzJdVsFdkAkZZbZXHrptBDx4DLMH7FdGo1vlt03nvdT/krlo6Ss1f+qbvSWN/3A7angfoku/kQP4TkVsrYueeXPL3vQe0uwPhz1+au2t+/LE+iGWp2r3iuigzZUckxJePdrVCR3D4RW0PsT2f/XVLeRYwj1WwoMgOO0+S5T8Er2a19azOMGqBUpzrLJD28dEtP9JXXHZKM1svjloH2jIrMWuzQ4+8Vq6wQO9KOEjMLPJG3GwPWsrajC1wBnURPvFR0bABBAgjZgrVL9Q24hWg1KUTorOpuAwPist2mpRUp1CNrSeo9VsS1Cb+sPzKZAzGLeIxH0TLQJ7VFK7amEIi1qz112gEA69YWkoGQuZGJMwprnJ7Wpj2pRyN7kIttTGdQx8MAidU4FCLU7E7MemHmEUKwHbBrPHxx9VnnN/Fk6sfCSj9vdh4fRBXsjTceA461ogZ8hE4n5ZO0+o+6leIgbE1owYOZ6/ZPdiefvyVCI+ocAPeUpVDjHH31SiXAahPnCjpvlxdx85QCNrPl2iNUDwlELJhJ95BCWY1Dx6TA6IxZjABOWCL0BbNJd72s+WTJwERjOIGI2YrRud81zACDJl2pwaIyGsZZLKWB7XPaTOqccABkBhhktJdY03OdqkAcBOveSSpsukHdMbD4OSTYdtPi5JcdRxa0PzPufsqNVT1nydwVZyqjNIjUb1M4KJrZKIpasVZ9NzKjHFr2EOaRmCDIK+g+yHaFluszarYDvy1G/peMxwOBG4hfPNXYj/YTtIbFamuJ/BfDKw2D+F/FpM8C5K1aHT8Wd6qtVGtSlE3QRIxB2KrUYoNGqMgDa7No4jmorO/36IxXpoPaqJaZHNI1ReLsm0VBaKeCloVgeKVUYIBMReNEU6ktbAJM7CTj6o1dVaWhUr9sRqPGgMYPTHFR3KXMhr9eIjqOK58E+zStCirLxlZMqOnAY8EBkU7U7AoTXl0honKdgBnNXrfWAkEx7yG/wCqr3U+3Gs4WWW03SyoZb8uATE4d7AnEY45poK2JOVIpW+5nta4teO6xrn4zg4gDDONIiYWSt5hgb7mMVu+09nFFh0nEvcNHM6MYHI5gQeYXO71dkN2PvwV4rZmnLRYpnBu5jerQnsjSPEnkM/JNpDAfys8o+qdTGLj+13UH6hMKhodiTu64n1CgnuuPLp91I858Pp9FA7/AIzxHQg+SKAzyynGeHTBHKbe4RsxHLUgVjz97lpLEcCNYEdc+hXSDDgsXa45gYgYxrI1jhpY+431x2bRYBGPluG+Naw1w2c/OA1AyDs0ch4gQuiXfjgNeXP2FNlY8F/BJWf9IPZXqNMXzR//2Q==",
            },
        ]
    }

    componentDidMount() {
        const url = "http://110.74.194.124:15011/v1/api/articles?page=1&limit=15";
        Axios.get(url)
        .then(response => {
            const listArticles = response.data.DATA;
            this.setState({ isLoading: false, articles:  [ ...this.state.articles, ...listArticles]});
        })
        .catch(error => console.log(error));
    }

    handleDelete = (ID) => {
        const url = `http://110.74.194.124:15011/v1/api/articles/${ID}`;
        Axios.delete(url)
        .then(response => {
            alert(response.data.MESSAGE);
            this.componentDidMount();
        })
        .catch(error => console.log(error));
    }

    render() {
        const { isLoading, articles } = this.state;
        if(isLoading) {
            return <h1 className="text-center">Loading...</h1>
        }
        return(
            <div className="px-5">
                <h3 className="text-center">Article Management</h3>
                <div className="text-center my-3">
                <Link to="/articles/add">
                    <Button variant="dark">Add New Article</Button>
                </Link>
                </div>
                <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>TITLE</th>
                    <th>DESCRIPTION</th>
                    <th>CREATED DATE</th>
                    <th>IMAGE</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                {articles.map(article => (
                    <tr key={article.ID}>
                    <td>{article.ID}</td>
                    <td className="w-25">
                        <p className="overFlowTitle">{article.TITLE}</p>
                    </td>
                    <td className="w-25">
                        <p className="overFlowDescription">{article.DESCRIPTION}</p>
                    </td>
                    <td>
                        {moment(article.CREATED_DATE).format("Y-M-D")}
                    </td>
                    <td style={{width: "120px"}}>
                        <Image rounded className="w-100" src = {article.IMAGE} alt={article.ID}/>
                    </td>
                    <td>
                        <Link to={`/articles/view/${article.ID}`}> 
                        <Button variant="primary">View</Button>
                        </Link>
                        <Link className="mx-1" to={`/articles/edit/${article.ID}`}> 
                        <Button variant="warning">Edit</Button>
                        </Link>
                        <Button variant="danger" onClick={() => this.handleDelete(article.ID)}>Delete</Button>
                    </td>
                    </tr>
                ))}
                </tbody>
            </Table>
            </div>
        );
    }
}


