import React, { Component } from 'react';
import Axios from "axios";
import randomString from "randomstring";
import { Form, Button, Image } from "react-bootstrap";
import "../../styles.css";

export class AddArticle extends Component {
    state = {
        ID: "",
        TITLE: "",
        DESCRIPTION: "",
        IMAGE: null,
        CREATED_DATE: "",
        titleError: "",
        descriptionError: "",
    }

    onChange = (event) => {
        const name = event.target.name;
        let value = event.target.value;
        if(event.target.type === "file") {
            value = event.target.files[0].name;
            this.setState({ image: value });
        }
        else {
            this.setState({[name]: value});
        }
    }

    handleAdd = (article) => {
        const url = "http://110.74.194.124:15011/v1/api/articles";
        Axios.post(url, article)
        .then(response => {
            alert(response.data.MESSAGE);
        })
        .catch(error => console.log(error));
    }

    validate = () => {
        const { TITLE, DESCRIPTION } = this.state;
        let titleError = "";
        let descriptionError = "";
        
        // title can't be empty
        if (TITLE === "") {
            titleError = "Title can't be empty!";
        }
        
        // description can't be empty
        if (DESCRIPTION === "") {
            descriptionError = "Description can't be empty!";
        }
    
        // error state will occur if the following is true
        if (titleError || descriptionError) {
            this.setState({ titleError, descriptionError });
            return false;
        }
        // else return true
        return true;
    };

    onSubmit = (event) => {
        event.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            let article = {};
            article.ID = randomString.generate(5);
            article.TITLE = this.state.title;
            article.DESCRIPTION = this.state.description;
            article.CREATED_DATE = new Date().toISOString();
            article.IMAGE = this.state.image;
            this.handleAdd(article);
            this.setState({ 
                ID: "",
                TITLE: "",
                DESCRIPTION: "",
                IMAGE: null,
                CREATED_DATE: "",
                titleError: "", 
                descriptionError: ""
            });
        }
    }

    render() {
        const { TITLE, DESCRIPTION, titleError, descriptionError } = this.state;
        return (
            <div className="container">
                <h3>Add Article</h3>
                <Form onSubmit={this.onSubmit}>

                    <Form.Group controlId="formBasicTitle">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" 
                            placeholder="Enter title" 
                            name="TITLE"
                            value={TITLE} 
                            onChange={this.onChange}/>
                        <h5 className="text-danger">{titleError}</h5>
                    </Form.Group>

                    <Form.Group controlId="formBasicDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" 
                            placeholder="Enter description" 
                            name="DESCRIPTION"
                            value={DESCRIPTION} 
                            onChange={this.onChange}/>
                        <h5 className="text-danger">{descriptionError}</h5>
                    </Form.Group>

                    <Form.Group className="image-upload">
                        <label for="imageFile">
                            <Image rounded className="w-25 rounded circle float-left mb-3" src="../../../img/placeholder-logo-img.png" alt="Label-Img" />
                        </label>
                        <Form.File name="IMAGE" type="file" id="imageFile" onChange={this.onChange}/>
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>
        );
    }
}

export default AddArticle;
