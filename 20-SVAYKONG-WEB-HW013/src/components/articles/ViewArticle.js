import React, { Component } from 'react';
import Axios from "axios";
import { Image } from "react-bootstrap";

export default class ViewArticle extends Component {
  state = {
    isLoading: true,
    article: {}
  }

  componentWillMount() {
    const { match } = this.props;
    let { id } = match.params;
    const url = `http://110.74.194.124:15011/v1/api/articles/${id}`;
    Axios.get(url)
      .then(response => { 
          console.log(response.data.DATA);
          const article = response.data.DATA;
          this.setState({ article });
      })
      .catch(error => console.log(error));

    // const tmpArticle = { 
    //   ID: 123456, 
    //   TITLE: "Nancy", 
    //   DESCRIPTION: "Korean k-pop star", 
    //   CREATED_DATE: "2020-06-11T03:56:13.324Z", 
    //   IMAGE: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSKhYdsTSKPcmiaJp1HlqcHkJhf88GWG1ziIMcVS_oOdZqtpcEA&usqp=CAU",
    // }
    // this.setState({ article: tmpArticle });
  }

  render() {
    const { isLoading, article } = this.state;
    if(isLoading) {
      return <h1 className="text-center">Loading...</h1>
    }
    return (
      <div className="container">
        <h3>Article</h3>
        <Image rounded className="w-25 rounded circle float-left mr-3" src={article.IMAGE} alt={article.ID} />
        <h1>{article.TITLE}</h1>
        <p>{article.DESCRIPTION}</p>
      </div>
    );
  }
}

