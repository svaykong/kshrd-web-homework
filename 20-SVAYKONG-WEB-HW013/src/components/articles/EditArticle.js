import React, { Component } from 'react';
import { Form, Button, Image } from "react-bootstrap";
import Axios from "axios";
import "../../styles.css";

export class EditArticle extends Component {
    state = {
        isLoading: true,
        TITLE: "",
        DESCRIPTION: "",
        IMAGE: null,
        CREATED_DATE: "",
        titleError: "",
        descriptionError: "",
    }
    
    componentWillMount() {
        const { match } = this.props;
        let { id } = match.params;
        const url = `http://110.74.194.124:15011/v1/api/articles/${id}`;
        Axios.get(url)
            .then(response => { 
                console.log(response.data.DATA);
                const article = response.data.DATA;
                this.setState({ title: article.TITLE, description: article.DESCRIPTION, image: article.IMAGE });
            })
            .catch(error => console.log(error));

        // const tmpArticle = { 
        //     ID: 123456, 
        //     TITLE: "Nancy", 
        //     DESCRIPTION: "Korean k-pop star", 
        //     CREATED_DATE: "2020-06-11T03:56:13.324Z", 
        //     IMAGE: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSKhYdsTSKPcmiaJp1HlqcHkJhf88GWG1ziIMcVS_oOdZqtpcEA&usqp=CAU",
        //   }
        // this.setState({ TITLE: tmpArticle.TITLE, DESCRIPTION: tmpArticle.DESCRIPTION, IMAGE: tmpArticle.IMAGE });
    }

    onChange = (event) => {
        const name = event.target.name;
        let value = event.target.value;
        if(event.target.type === "file") {
            value = event.target.files[0].name;
            this.setState({ image: value });
        }
        else {
            this.setState({[name]: value});
        }
    }

    handleUpdate = (article, ID) => {
        const url = `http://110.74.194.124:15011/v1/api/articles/${ID}`;
        Axios.put(url, article)
        .then(response => {
            alert(response.data.MESSAGE);
        })
        .catch(error => console.log(error));
    }

    validate = () => {
        const { TITLE, DESCRIPTION } = this.state;
        let titleError = "";
        let descriptionError = "";
        
        // title can't be empty
        if (TITLE === "") {
            titleError = "Title can't be empty!";
        }
        
        // description can't be empty
        if (DESCRIPTION === "") {
            descriptionError = "Description can't be empty!";
        }
    
        // error state will occur if the following is true
        if (titleError || descriptionError) {
            this.setState({ titleError, descriptionError });
            return false;
        }
        // else return true
        return true;
    };

    onSubmit = (event) => {
        event.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const { match } = this.props;
            let { id } = match.params;
            let article = {};
            article.TITLE = this.state.title;
            article.DESCRIPTION = this.state.description;
            article.IMAGE = this.state.image;
            this.handleUpdate(article, id);
            this.setState({ titleError: "", descriptionError: "" });
        }
    }

    render() {
        const { isLoading, TITLE, DESCRIPTION, IMAGE, titleError, descriptionError } = this.state;
        if(isLoading) {
            return <h1 className="text-center">Loading...</h1>
        }
        return (
            <div className="container">
                <h3>Update Article</h3>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="formBasicTitle">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" 
                            placeholder="Enter title" 
                            name="TITLE"
                            value={TITLE} 
                            onChange={this.onChange}/>
                        <h5 className="text-danger">{titleError}</h5>
                    </Form.Group>

                    <Form.Group controlId="formBasicDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" 
                            placeholder="Enter description" 
                            name="DESCRIPTION"
                            value={DESCRIPTION} 
                            onChange={this.onChange}/>
                        <h5 className="text-danger">{descriptionError}</h5>
                    </Form.Group>

                    <Form.Group className="image-upload">
                        <label for="imageFile">
                            <Image rounded className="w-70 rounded circle float-left mb-3" src={IMAGE} alt={TITLE} />
                        </label>
                        <Form.File name="IMAGE" type="file" id="imageFile" onChange={this.onChange}/>
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Save
                    </Button>
                </Form>
            </div>
        );
    }
}

export default EditArticle;
